# Company-Office React - Redux
this project for testing only using React + Redux (basic)

For Glints - Frontend_Code_Challenge in 5 days. (3 hour per day)

Using [STARTER KIT by davezuko](https://github.com/davezuko/react-redux-starter-kit) as recomemded, thanks

## Requirements
* node `^5.0.0`

## Installation && Running App

After confirming that your environment meets the above [requirements](#requirements), you can create a new project based on `react-redux-starter-kit` by doing the following:

```bashh
$ git clone https://gitlab.com/arhamarifuddin/glints-fe-code-challenge.git <project-name>
$ cd <project-name>
$ npm i
$ npm start (running)
```

## Thank You

may some requirement test are miss :)
not enough time for research more

## Other Project Experience

* [project 1 (2018) : React + Redux + Firestore + semantic-ui ](https://siaga-47d08.firebaseapp.com/)
* [project 2 (2017) : React + Firebase-Realtime + semantic-ui ](https://urban-gigs.firebaseapp.com/) (credential sent)
